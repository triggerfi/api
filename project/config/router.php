<?php
$router = $di->get('router');

$api = new \Phalcon\Mvc\Router\Group();
$api->setPrefix('/v1');

$uuidPattern = '([0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12})';

/*
 * TRIGGERS
 */
$api->addGet('/triggers', [
    'namespace' => 'App\\Trigger\\Api\\Controller',
    'controller' => 'trigger',
    'action' => 'list'
]);
$api->addGet('/triggers/:int/conditions', [
    'namespace' => 'App\\Trigger\\Api\\Controller',
    'controller' => 'trigger',
    'action' => 'conditions',
    'triggerId' => 1
]);
$api->addPost('/triggers/:int', [
    'namespace' => 'App\\Trigger\\Api\\Controller',
    'controller' => 'trigger',
    'action' => 'fire',
    'triggerId' => 1
]);

/*
 * CONDITIONS
 */
$api->addGet('/conditions', [
    'namespace' => 'App\\Conditions\\Api\\Controller',
    'controller' => 'conditions',
    'action' => 'all'
]);

$api->addGet('/conditions/groups', [
    'namespace' => 'App\\Conditions\\Api\\Controller',
    'controller' => 'conditions',
    'action' => 'groups'
]);

/*
 * VARIABLES
 */
$api->addGet('/variables', [
    'namespace' => 'App\\Variable\\Api\\Controller',
    'controller' => 'variable',
    'action' => 'all'
]);

$api->addGet('/variables/' . $uuidPattern, [
    'namespace' => 'App\\Variable\\Api\\Controller',
    'controller' => 'variable',
    'action' => 'read',
    'uuid' => 1
]);

$api->addDelete('/variables/' . $uuidPattern, [
    'namespace' => 'App\\Variable\\Api\\Controller',
    'controller' => 'variable',
    'action' => 'delete',
    'uuid' => 1
]);

$api->addPost('/variables', [
    'namespace' => 'App\\Variable\\Api\\Controller',
    'controller' => 'variable',
    'action' => 'create'
]);

/*
 * ACTIONS
 */
$api->addGet('/actions', [
    'namespace' => 'App\\Action\\Api\\Controller',
    'controller' => 'action',
    'action' => 'list'
]);

/*
 * AUTOMATIONS
 */
$api->addGet('/automations', [
    'namespace' => 'App\\Automation\\Api\\Controller',
    'controller' => 'automation',
    'action' => 'list'
]);


$api->addGet('/automations/' . $uuidPattern . '', [
    'namespace' => 'App\\Automation\\Api\\Controller',
    'controller' => 'automation',
    'action' => 'get',
    'automationId' => 1
]);

$api->addPost('/automations', [
    'namespace' => 'App\\Automation\\Api\\Controller',
    'controller' => 'automation',
    'action' => 'create'
]);


/*
 * DATASETS
 */
$api->addGet('/datasets', [
    'namespace' => 'App\\Automation\\Api\\Controller',
    'controller' => 'dataset',
    'action' => 'list'
]);

$api->addGet('/datasets/' . $uuidPattern . '/documents', [
    'namespace' => 'App\\Automation\\Api\\Controller',
    'controller' => 'dataset',
    'action' => 'documents',
    'uuid' => 1
]);

$api->addPost('/datasets', [
    'namespace' => 'App\\Automation\\Api\\Controller',
    'controller' => 'dataset',
    'action' => 'create'
]);

/*
 * VERSION
 */
$api->addGet('/version', [
    'namespace' => 'App\\Version\\Api\\Controller',
    'controller' => 'version',
    'action' => 'index'
]);

$api->addGet('/test', [
    'namespace' => 'App\\Version\\Api\\Controller',
    'controller' => 'version',
    'action' => 'test'
]);


/** @var \Phalcon\Mvc\Router $router */
$router->mount($api);
