<?php

use Phalcon\Mvc\View;
use Phalcon\Mvc\View\Engine\Php as PhpEngine;
use Phalcon\Mvc\Url as UrlResolver;
use Phalcon\Mvc\View\Engine\Volt as VoltEngine;
use Phalcon\Mvc\Model\Metadata\Memory as MetaDataAdapter;
use Phalcon\Session\Adapter\Files as SessionAdapter;

/**
 * Shared configuration service
 */
$di->setShared('config', function () {
    return include BASE_PATH . "/config/config.php";
});

/**
 * Entities
 */
$di->setShared('entities', function () {
    return include BASE_PATH . "/config/entities.php";
});

$di->setShared('repositories', function () {
    return include BASE_PATH . "/config/repositories.php";
});

/**
 * The URL component is used to generate all kind of urls in the application
 */
$di->setShared('url', function () {
    $config = $this->getConfig();

    $url = new UrlResolver();
    $url->setBaseUri($config->application->baseUri);

    return $url;
});

/**
 * Setting up the view component
 */
$di->setShared('view', function () {
    $config = $this->getConfig();

    $view = new View();
    $view->setDI($this);
    $view->setViewsDir($config->application->viewsDir);

    $view->registerEngines([
        '.volt' => function ($view) {
            $config = $this->getConfig();

            $volt = new VoltEngine($view, $this);

            $volt->setOptions([
                'compiledPath' => $config->application->cacheDir,
                'compiledSeparator' => '_'
            ]);

            return $volt;
        },
        '.phtml' => PhpEngine::class

    ]);

    return $view;
});

$di->setShared('dbal', function () use ($di) {
    /** @var \stdClass $configData */
    $configData = $this->getConfig()->database;

    $config = new \Doctrine\DBAL\Configuration();
    $connectionParams = [
        'url' => 'mysql://' . $configData->username . ':' . $configData->password . '@' . $configData->host
            . '/' . $configData->dbname,
    ];

    $conn = \Doctrine\DBAL\DriverManager::getConnection($connectionParams, $config);
    $conn->setFetchMode(PDO::FETCH_OBJ);

    return $conn;
});

/**
 * Database connection is created based in the parameters defined in the configuration file
 */
$di->setShared('mongo', function () use ($di) {
    $configData = $this->getConfig()->mongo;
    $client = new \MongoDB\Client('mongodb://' . $configData->host . ':' . $configData->port);
    return $client->selectDatabase($configData->dbname);
});

/**
 * If the configuration specify the use of metadata adapter use it or use memory otherwise
 */
$di->setShared('modelsMetadata', function () {
    return new MetaDataAdapter();
});

/**
 * Start the session the first time some component request the session service
 */
$di->setShared('session', function () {
    $session = new SessionAdapter();
    $session->start();

    return $session;
});

$di->setShared('dispatcher', function () use ($di) {
    $eventsManager = new \Phalcon\Events\Manager();
    $eventsManager->attach('dispatch:beforeException', function ($event, $dispatcher, $exception) use ($di) {
        /**
         * @var \Phalcon\Mvc\Dispatcher $dispatcher
         * @var \Exception              $exception
         * @var \Phalcon\Events\Event   $event
         */
        if ($event->getType() == 'beforeException') {
            switch ($exception->getCode()) {
                // 404 error
                case \Phalcon\Dispatcher::EXCEPTION_HANDLER_NOT_FOUND:
                case \Phalcon\Dispatcher::EXCEPTION_ACTION_NOT_FOUND:
                    $dispatcher->forward([
                        'namespace' => 'App\\Common\\Api\\Controller',
                        'controller' => 'errors',
                        'action' => 'notFound'
                    ]);

                    return false;

                default:
                    // 500 error
                    $dispatcher->forward([
                        'namespace' => 'App\\Common\\Api\\Controller',
                        'controller' => 'errors',
                        'action' => 'exception',
                        'params' => [
                            'exception' => $exception
                        ]
                    ]);

                    return false;
            }
        }

        return true;
    });

    $dispatcher = new \Phalcon\Mvc\Dispatcher();
    $dispatcher->setEventsManager($eventsManager);
    return $dispatcher;
});

/*
 * Event Dispatcher
 */
$di->setShared('ed', function () use ($di) {
    return new \App\Common\Event\Dispatcher();
});
