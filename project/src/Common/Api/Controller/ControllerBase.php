<?php

namespace App\Common\Api\Controller;

use Phalcon\Mvc\Controller;

class ControllerBase extends Controller
{
    /**
     * @var \App\Component\Api\Response
     */
    public $api;

    /**
     * @var \App\Component\Command\CommandBus
     */
    public $commandBus;

    /**
     * initialize controller
     */
    public function initialize()
    {
        $this->api = new \App\Component\Api\Response();
        $this->commandBus = new \App\Component\Command\CommandBus($this->di);

        $origin = $this->request->getHeader("ORIGIN") ? $this->request->getHeader("ORIGIN") : '*';
        $this->response->setHeader("Access-Control-Allow-Origin", $origin)
            ->setHeader("Access-Control-Allow-Methods", 'GET,PUT,POST,DELETE,OPTIONS')
            ->setHeader(
                "Access-Control-Allow-Headers",
                'Origin, X-Requested-With, Content-Range, Content-Disposition, Content-Type, Authorization'
            )
            ->setHeader("Access-Control-Allow-Credentials", true);
    }

    /**
     * This method returns proper json object for API calls
     *
     * @return \Phalcon\Http\Response|\Phalcon\Http\ResponseInterface
     */
    public function sendApiResponse(): \Phalcon\Http\ResponseInterface
    {
        $this->response->setContentType('application/json', 'UTF-8');
        $this->response->setJsonContent($this->api);
        return $this->response;
    }

    /**
     * @param int $code
     *
     * @return \Phalcon\Http\Response|\Phalcon\Http\ResponseInterface
     */
    public function respond(int $code = 200)
    {
        return $this->response->setStatusCode($code)->setJsonContent($this->api);
    }

    /**
     * no access
     *
     * @return bool
     */
    public function noAccess(): bool
    {
        $this->dispatcher->forward([
            'controller' => 'errors',
            'action' => 'forbidden'
        ]);

        return true;
    }

    /**
     * @return \Phalcon\Http\ResponseInterface
     */
    public function notFound(): \Phalcon\Http\ResponseInterface
    {
        $error = new \App\Component\Api\Error();
        $error->setStatus($error::ERROR_NOT_FOUND);
        $this->api->setError($error);

        return $this->respond(404);
    }

    /**
     * @param \Exception $e
     *
     * @return \Phalcon\Http\ResponseInterface
     */
    public function error500(\Exception $e): \Phalcon\Http\ResponseInterface
    {
        $error = new \App\Component\Api\Error();
        $error->setStatus($error::ERROR_INTERNAL);
        if (!isProd()) {
            $error->setTitle($e->getMessage());
            $error->setDetail($e->getTraceAsString());
        }
        $error->setCode(500);
        $this->api->setError($error);

        return $this->respond(500);
    }

    /**
     * method not allowed
     *
     * @return bool
     */
    public function methodNotAllowed(): bool
    {
        $this->dispatcher->forward(['controller' => 'errors', 'action' => 'methodNotAllowed']);
        return true;
    }
}