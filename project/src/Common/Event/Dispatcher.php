<?php

namespace App\Common\Event;

class Dispatcher
{
    protected $listeners = [];
    protected $events = [];

    /*
     * dispatcher events of $eventName listeners
     */
    public function dispatch(string $eventName, EventInterface $event): void
    {
        if (empty($this->listeners[$eventName])) {
            return;
        }

        foreach ($this->listeners[$eventName] as $v) {
            $obj = new $v;
            $obj->handle($event);
            unset($obj);
        }
    }

    /*
     * adds event to the list
     */
    public function apply(EventInterface $event): void
    {
        $this->events[get_class($event)][] = $event;
    }

    /*
     * adds listener to the list
     */
    public function addListener(string $eventName, string $listener): void
    {
        $this->listeners[$eventName][] = $listener;
    }

    /*
     * fire all event listeners
     */
    public function fire()
    {
        foreach ($this->events as $eventName => $events) {
            foreach ($events as $event) {
                $this->dispatch($eventName, $event);
            }
        }
    }
}
