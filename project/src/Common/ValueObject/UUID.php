<?php

namespace App\Common\ValueObject;

use Phalcon\Security\Random;

final class UUID
{
    /** @var string */
    protected $uuid;

    public function __construct(string $uuid = null)
    {
        if (preg_match('/([0-9a-f]{8})-([0-9a-f]{4})-([0-9a-f]{4})-([0-9a-f]{4})-([0-9a-f]{12})/', $uuid)) {
            $this->uuid = $uuid;
        } elseif (preg_match('/([0-9a-f]{8})([0-9a-f]{4})([0-9a-f]{4})([0-9a-f]{4})([0-9a-f]{12})/', $uuid)) {
            $this->uuid = preg_replace(
                '/([0-9a-f]{8})([0-9a-f]{4})([0-9a-f]{4})([0-9a-f]{4})([0-9a-f]{12})/',
                '$1-$2-$3-$4-$5',
                $uuid
            );
        } else {
            $this->uuid = self::fromBin($uuid);
        }

        if ($uuid === null) {
            $this->uuid = self::generate();
        }
    }

    public function getBin(): string
    {
        return self::toBin($this->uuid);
    }

    public function getString(): string
    {
        return $this->uuid;
    }

    public function getSimplified(): string
    {
        return str_replace('-', '', $this->uuid);
    }

    public static function generate(): string
    {
        try {
            return (new Random())->uuid();
        } catch (\Exception $e) {
            // log shit
            return '';
        }
    }

    public static function fromBin($uuid)
    {
        return preg_replace(
            '/([0-9a-f]{8})([0-9a-f]{4})([0-9a-f]{4})([0-9a-f]{4})([0-9a-f]{12})/',
            '$1-$2-$3-$4-$5',
            bin2hex($uuid)
        );
    }

    public static function toBin($uuid): string
    {
        return hex2bin(str_replace('-', '', $uuid));
    }
}
