<?php

namespace App\Common\Repository;

use Phalcon\Di\Injectable;

final class Factory extends Injectable
{
    /**
     * @param string $name
     *
     * @return \App\Common\Repository\RepositoryInterface
     */
    public static function get(string $name)
    {
        $repositories = self::getDI()->get('config');
    }
}
