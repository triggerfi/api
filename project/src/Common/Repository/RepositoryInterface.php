<?php

namespace App\Common\Repository;

interface RepositoryInterface extends \Countable, \IteratorAggregate, \ArrayAccess
{
    public function add($element);

    public function remove($element);

    public function clear();

    public function contains($element);

    public function get($key);

    public function toArray();

    public function slice($offset, $length = null);
}
