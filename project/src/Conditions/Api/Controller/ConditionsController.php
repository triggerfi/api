<?php

namespace App\Conditions\Api\Controller;

use App\Common\Api\Controller\ControllerBase;

final class ConditionsController extends ControllerBase
{
    /*
     * Conditions list for the company
     */
    public function allAction(): \Phalcon\Http\ResponseInterface
    {
        try {
            $query = new \App\Conditions\Domain\Query\GetListOfConditions();
            $handler = new \App\Conditions\Domain\Query\GetListOfConditionsHandler($this->di);
            $conditions = $handler->handle($query);
            $this->response->setJsonContent(['data' => $conditions]);
        } catch (\Exception $e) {
            return $this->error500($e);
        }

        return $this->response;
    }

    /*
     * Conditions
     */
    public function groupsAction(): \Phalcon\Http\ResponseInterface
    {
        try {
            $query = new \App\Conditions\Domain\Query\GetListOfConditionsGroups();
            $handler = new \App\Conditions\Domain\Query\GetListOfConditionsGroupsHandler($this->di);
            $conditions = $handler->handle($query);
            $this->response->setJsonContent(['data' => $conditions]);
        } catch (\Exception $e) {
            return $this->error500($e);
        }

        return $this->response;
    }
}