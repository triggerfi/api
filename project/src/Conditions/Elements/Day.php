<?php

namespace App\Conditions\Elements;

class Day extends AbstractCondition
{
    /**
     * Date constructor.
     *
     * @param int    $id
     * @param string $name
     */
    public function __construct(int $id, string $name)
    {
        parent::__construct($id, $name);
        $this->signs = [
            self::SIGN_NE,
            self::SIGN_EQ
        ];

        $this->values = [
            'Monday',
            'Tuesday',
            'Wednesday',
            'Thursday',
            'Friday',
            'Saturday',
            'Sunday'
        ];
    }
}