<?php

namespace App\Conditions\Elements;


class VariableValueDiff extends AbstractCondition
{
    public function __construct(int $id, string $name)
    {
        parent::__construct($id, $name);
        $this->name = 'Variable\'s value difference';
        $this->signs = [
            self::SIGN_LTE,
            self::SIGN_LT,
            self::SIGN_GTE,
            self::SIGN_GT,
            self::SIGN_NE,
            self::SIGN_EQ
        ];

        $this->values = '';
    }
}