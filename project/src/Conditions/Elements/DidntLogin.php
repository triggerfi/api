<?php

namespace App\Conditions\Elements;

class DidntLogin extends AbstractCondition
{
    public function __construct(int $id, string $name)
    {
        parent::__construct($id, $name);
        $this->signs = [
            self::SIGN_LTE,
            self::SIGN_LT,
            self::SIGN_GTE,
            self::SIGN_GT,
            self::SIGN_NE,
            self::SIGN_EQ
        ];

        $this->name = 'Days since last log in';
    }
}