<?php

namespace App\Conditions\Elements;

/**
 * Class LastName
 *
 * @package App\Conditions\Elements
 */
class LastName extends AbstractCondition
{
    /**
     * LastName constructor.
     *
     * @param int    $id
     * @param string $name
     */
    public function __construct(int $id, string $name)
    {
        parent::__construct($id, $name);
        $this->signs = [
            self::SIGN_NE,
            self::SIGN_EQ
        ];
    }
}