<?php

namespace App\Conditions\Elements;

class Email extends AbstractCondition
{
    public function __construct(int $id, string $name)
    {
        parent::__construct($id, $name);

        $this->signs = [
            self::SIGN_NE,
            self::SIGN_EQ
        ];
    }
}