<?php

namespace App\Conditions\Elements;

use App\Conditions\Exception\InvalidCondition;

/**
 * Class ConditionFactory
 *
 * @package App\Conditions\Elements
 */
class ConditionFactory
{
    const TYPE_DATE = 'Date';
    const TYPE_TIME = 'Time';
    const TYPE_DIDNT_LOGIN = 'DidntLogin';
    const TYPE_DAY = 'Day';
    const TYPE_EMAIL = 'Email';
    const TYPE_COUNTRY = 'Country';
    const TYPE_EMPLOYER = 'Employer';
    const TYPE_MERCHANT = 'Merchant';
    const TYPE_DOB = 'DOB';
    const TYPE_FNAME = 'FirstName';
    const TYPE_LNAME = 'LastName';
    const TYPE_AMOUNT = 'Amount';
    const TYPE_VARIABLE = 'Variable';
    const TYPE_VARIABLE_VALUE = 'VariableValue';
    const TYPE_VARIABLE_VALUE_DIFF = 'VariableValueDiff';

    /**
     * @param int    $id
     * @param string $type
     *
     * @return \App\Conditions\Elements\AbstractCondition
     * @throws \App\Conditions\Exception\InvalidCondition
     */
    public static function create(int $id, string $type): AbstractCondition
    {
        $type = 'self::TYPE_' . $type;
        if (!defined($type)) {
            throw new InvalidCondition('Unknown condition type ' . $type);
        }

        $className = '\\App\\Conditions\\Elements\\' . constant($type);
        $object = new $className($id, constant($type));

        return $object;
    }
}
