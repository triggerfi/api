<?php

namespace App\Conditions\Elements;

/**
 * Class Country
 *
 * @package App\Conditions\Elements
 */
class Country extends AbstractCondition
{
    /**
     * Country constructor.
     *
     * @param int    $id
     * @param string $name
     */
    public function __construct(int $id, string $name)
    {
        parent::__construct($id, $name);

        $this->signs = [
            self::SIGN_EQ,
            self::SIGN_NE
        ];

        $this->values = [
            'pl' => 'Poland',
            'mt' => 'Malta',
            'fi' => 'Finland',
            'se' => 'Sweden'
        ];
    }
}