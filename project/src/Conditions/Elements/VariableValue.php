<?php

namespace App\Conditions\Elements;

use Cerebro\Api\Models\TriggersVariables;

class VariableValue extends AbstractCondition
{
    public function __construct(int $id, string $name)
    {
        parent::__construct($id, $name);
        $this->name = 'Variable\'s current value';
        $this->signs = [
            self::SIGN_LTE,
            self::SIGN_LT,
            self::SIGN_GTE,
            self::SIGN_GT,
            self::SIGN_NE,
            self::SIGN_EQ
        ];

        $this->values = '';
    }
}