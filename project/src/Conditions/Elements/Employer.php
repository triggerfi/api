<?php

namespace App\Conditions\Elements;

class Employer extends AbstractCondition
{
    public function __construct(int $id, string $name)
    {
        parent::__construct($id, $name);

        $this->signs = [
            self::SIGN_EQ,
            self::SIGN_NE
        ];

        $this->values = [
            'Betsson',
            'StarsGroup',
            'Gaming Innovation Group',
            'Gamma Entert',
            'VideoSlots',
            'Tipico',
            'Malta Public Transport',
            'GO',
            'Central Bank',
            'BOV',
            'Besedo',
            'Sata Bank',
            'FIMBank',
            'Mapfre',
            'Sovereign Group',
            'Meridian',
            'Corinthia',
            'Hugo\'s Group',
            'Palace',
            'AX',
            'DB Goup',
            'Nestle'
        ];
    }
}