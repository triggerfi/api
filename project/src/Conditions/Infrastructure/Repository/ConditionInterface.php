<?php

namespace App\Conditions\Infrastructure\Repository;

use App\Conditions\Domain\Model\Condition;
use App\Common\ValueObject\UUID;

interface ConditionInterface
{
    public function findByUuid(UUID $uuid): Condition;

    public function add(Condition $condition): void;

    public function remove(Condition $automation): void;

    public function findAll();
}
