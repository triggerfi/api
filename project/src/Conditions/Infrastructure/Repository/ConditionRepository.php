<?php


namespace App\Conditions\Infrastructure\Repository;

use App\Conditions\Domain\Model\Condition;
use App\Common\ValueObject\UUID;
use App\Conditions\Domain\Model\ConditionGroup;
use App\Conditions\Exception\ConditionRepositoryException;
use Doctrine\DBAL\Connection;

final class ConditionRepository implements ConditionInterface
{
    /** @var \App\Conditions\Domain\Model\Condition[] */
    private $automations = [];

    /** @var \Doctrine\DBAL\Connection */
    private $em;

    public function __construct(Connection $manager)
    {
        $this->em = $manager;
    }

    public function remove(Condition $automation): void
    {
        // TODO: Implement remove() method.
    }

    /**
     * @return ConditionGroup[]
     */
    public function findGroups(): array
    {
        $groups = $this->em->createQueryBuilder()
            ->select('*')
            ->from('conditions_group')
            ->execute()
            ->fetchAll();

        $out = [];
        foreach ($groups as $k => $v) {
            $out[] = ConditionGroup::factory($v);
        }

        return $out;
    }

    public function findByUuid(UUID $uuid): Condition
    {
        $automation = $this->em->createQueryBuilder()
            ->select('*')
            ->from('automations')
            ->where('uuid = ?')
            ->setParameter(0, $uuid->getBin())
            ->execute()
            ->fetch();

        return Condition::factory($automation);
    }

    public function findAll(): array
    {
        try {
            $automations = $this->em->createQueryBuilder()
                ->select('*')
                ->from('conditions')
                ->execute()
                ->fetchAll();
        } catch (\Exception $e) {
            throw new ConditionRepositoryException('Error while searching', 0, $e);
        }

        foreach ($automations as $k => $v) {
            $e = Condition::factory($v);
            $e->setGroupId($v->conditions_group_id);
            $this->automations[] = $e;
        }

        return $this->automations;
    }

    public function add(Condition $condition): void
    {
        try {
            $this->em->createQueryBuilder()
                ->insert('automations')
                ->setValue('uuid', '?')
                ->setValue('trigger_id', '?')
                ->setValue('companies_id', 1)
                ->setValue('name', '?')
                ->setValue('data', '?')
                ->setParameter(0, $condition->getUuid()->getBin())
                ->setParameter(1, $condition->getTriggerId())
                ->setParameter(2, $condition->getName())
                ->setParameter(3, \json_encode($condition->getJson()))
                ->execute();
        } catch (\Exception $e) {
            throw new ConditionRepositoryException('Error saving entity', 0, $e);
        }
    }
}
