<?php

namespace App\Conditions\Domain\Model;

use Phalcon\Text;

class Condition
{
    const TYPE_STRING = 'string';
    const TYPE_DATE = 'date';
    const TYPE_NUMBER = 'number';
    const TYPE_DATASET = 'dataset';

    private $id;

    private $name;

    private $type;

    private $groupId;

    public static function factory(object $data): Condition
    {
        $obj = new Condition();
        foreach ($data as $k => $v) {
            $k = lcfirst(Text::camelize($k));
            if (method_exists($obj, 'set' . ucfirst($k))) {
                $obj->{'set' . ucfirst($k)}($v);
            }
        }

        return $obj;
    }

    /**
     * @return mixed
     */
    public function getGroupId()
    {
        return $this->groupId;
    }

    /**
     * @param mixed $groupId
     */
    public function setGroupId($groupId): void
    {
        $this->groupId = $groupId;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @param mixed $type
     */
    public function setType($type): void
    {
        $this->type = $type;
    }

    /*
     * returns possible comparing signs for specific type
     */
    public static function getSignsForType(string $type): array
    {
        switch ($type) {
            case self::TYPE_STRING:
                return ['eq', 'ne'];
            case self::TYPE_NUMBER:
                return ['eq', 'ne', 'gt', 'lt', 'gte', 'lte'];
            case self::TYPE_DATE:
                return ['eq', 'ne', 'gt', 'lt', 'gte', 'lte'];
            case self::TYPE_DATASET:
                return ['eq', 'ne', 'gt', 'lt', 'gte', 'lte'];
            default:
                throw new \Exception('Invalid condition type');
        }
    }
}
