<?php

namespace App\Conditions\Domain\Model;

use Phalcon\Text;

final class ConditionGroup
{
    private $id;

    private $name;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    public static function factory(object $data): ConditionGroup
    {
        $obj = new ConditionGroup();
        foreach ($data as $k => $v) {
            $k = lcfirst(Text::camelize($k));
            if (method_exists($obj, 'set' . ucfirst($k))) {
                $obj->{'set' . ucfirst($k)}($v);
            }
        }

        return $obj;
    }
}
