<?php

namespace App\Conditions\Domain\Query;

use App\Component\Query\QueryHandlerAbstract;
use App\Conditions\Infrastructure\Repository\ConditionRepository;
use Phalcon\Di;

class GetListOfConditionsHandler extends QueryHandlerAbstract
{
    private $conditionRepository;

    public function __construct(Di $di)
    {
        parent::__construct($di);
        $this->conditionRepository = new ConditionRepository($this->di->get('dbal'));
    }

    public function handle(GetListOfConditions $query): array
    {
        $output = [];

        $triggersEventsConditions = $this->conditionRepository->findAll();
        foreach ($triggersEventsConditions as $v) {
            $output[] = new ConditionView($v);
        }

        return $output;
    }
}
