<?php

namespace App\Conditions\Domain\Query;

use App\Conditions\Domain\Model\ConditionGroup;

final class ConditionGroupView implements \JsonSerializable
{
    /** @var int */
    private $id;

    /** @var string */
    private $name;


    public function __construct(ConditionGroup $group)
    {
        $this->id = $group->getId();
        $this->name = $group->getName();
    }

    public function jsonSerialize()
    {
        return [
            'id' => (int)$this->id,
            'name' => $this->name
        ];
    }
}
