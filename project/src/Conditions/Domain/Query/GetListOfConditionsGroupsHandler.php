<?php

namespace App\Conditions\Domain\Query;

use App\Component\Query\QueryHandlerAbstract;
use App\Conditions\Infrastructure\Repository\ConditionRepository;

class GetListOfConditionsGroupsHandler extends QueryHandlerAbstract
{
    private $conditionRepository;

    public function __construct(\Phalcon\Di $di)
    {
        parent::__construct($di);
        $this->conditionRepository = new ConditionRepository($this->di->get('dbal'));
    }

    public function handle(GetListOfConditionsGroups $query): array
    {
        $output = [];

        $groups = $this->conditionRepository->findGroups();

        foreach ($groups as $v) {
            $output[] = new ConditionGroupView($v);
        }

        return $output;
    }
}
