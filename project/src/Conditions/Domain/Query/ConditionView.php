<?php

namespace App\Conditions\Domain\Query;

use App\Conditions\Domain\Model\Condition;

final class ConditionView implements \JsonSerializable
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $group_id;

    /**
     * @var string
     */
    private $type;

    /** @var array */
    private $signs;


    public function __construct(Condition $condition)
    {
        $this->id = (int)$condition->getId();
        $this->name = $condition->getName();
        $this->group_id = (int)$condition->getGroupId();
        $this->type = $condition->getType();
        $this->signs = Condition::getSignsForType($condition->getType());
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'group_id' => $this->group_id,
            'type' => $this->type,
            'signs' => $this->signs,
            'values' => ''
        ];
    }


}