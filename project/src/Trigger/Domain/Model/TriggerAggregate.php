<?php

namespace App\Trigger\Domain\Model;

use Phalcon\Text;

final class TriggerAggregate
{
    /** @var int */
    private $id;

    /** @var string */
    private $name;

    /** @var \App\Trigger\Domain\Model\TriggerCondition[] */
    private $conditions;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return \App\Trigger\Domain\Model\TriggerCondition[]
     */
    public function getConditions(): array
    {
        return $this->conditions;
    }

    /**
     * @param \App\Trigger\Domain\Model\TriggerCondition $condition
     */
    public function addCondition(\App\Trigger\Domain\Model\TriggerCondition $condition): void
    {
        $this->conditions[] = $condition;
    }

    public static function factory(object $data): TriggerAggregate
    {
        $obj = new TriggerAggregate();
        foreach ($data as $k => $v) {
            $k = lcfirst(Text::camelize($k));
            if (method_exists($obj, 'set' . ucfirst($k))) {
                $obj->{'set' . ucfirst($k)}($v);
            }
        }

        return $obj;
    }
}
