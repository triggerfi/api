<?php

namespace App\Trigger\Domain\Model;

final class TriggerCondition
{
    /** @var int */
    private $id;

    /** @var int */
    private $triggerId;

    /** @var int */
    private $conditionsId;

    /** @var string */
    private $type;

    /** @var string */
    private $name;

    /** @var mixed */
    private $value;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type): void
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value): void
    {
        $this->value = $value;
    }

    /**
     * @return int
     */
    public function getTriggerId()
    {
        return $this->triggerId;
    }

    /**
     * @param int $triggerId
     */
    public function setTriggerId(int $triggerId): void
    {
        $this->triggerId = $triggerId;
    }

    /**
     * @return int
     */
    public function getConditionsId()
    {
        return $this->conditionsId;
    }

    /**
     * @param int $conditionsId
     */
    public function setConditionsId(int $conditionsId): void
    {
        $this->conditionsId = $conditionsId;
    }
}
