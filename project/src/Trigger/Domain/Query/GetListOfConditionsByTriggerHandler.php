<?php

namespace App\Trigger\Domain\Query;

use App\Component\Query\QueryHandlerAbstract;
use App\Trigger\Infrastructure\Repository\TriggerRepository;
use Phalcon\Di;

class GetListOfConditionsByTriggerHandler extends QueryHandlerAbstract
{
    private $triggersRepository;

    public function __construct(Di $di)
    {
        parent::__construct($di);
        $this->triggersRepository = new TriggerRepository($this->di->get('dbal'));
    }

    public function handle(GetListOfConditionsByTrigger $query): array
    {
        $output = [];

        $triggerAgg = $this->triggersRepository->findConditionsByTrigger($query->getTriggerId());

        foreach ($triggerAgg->getConditions() as $v) {
            $output[] = new TriggerConditionView($v);
        }

        return $output;
    }
}
