<?php


namespace App\Trigger\Domain\Query;

use App\Component\Query\QueryHandlerAbstract;
use App\Trigger\Infrastructure\Repository\TriggerRepository;
use Phalcon\Di;

final class GetListOfEventsHandler extends QueryHandlerAbstract
{
    private $triggersRepository;

    public function __construct(Di $di)
    {
        parent::__construct($di);
        $this->triggersRepository = new TriggerRepository($this->di->get('dbal'));
    }

    /**
     * @param \App\Trigger\Domain\Query\GetListOfTriggers $query
     *
     * @return \App\Trigger\Domain\Query\TriggerView[]
     */
    public function handle(GetListOfTriggers $query): array
    {
        $output = [];

        $data = $this->triggersRepository->findAll();

        foreach ($data as $v) {
            $output[] = new TriggerView($v->id, $v->name);
        }

        return $output;
    }
}
