<?php

namespace App\Trigger\Domain\Query;

use App\Conditions\Domain\Model\Condition;
use App\Trigger\Domain\Model\TriggerCondition;

final class TriggerConditionView implements \JsonSerializable
{
    private $id;

    private $name;

    private $value;

    private $signs;

    private $type;

    public function __construct(TriggerCondition $condition)
    {
        $this->id = (int)$condition->getId();
        $this->name = $condition->getName();
        $this->value = $condition->getValue();
        $this->type = $condition->getType();
        $this->signs = Condition::getSignsForType($condition->getType());
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'signs' => $this->signs,
            'type' => $this->type,
            'values' => $this->value
        ];
    }
}
