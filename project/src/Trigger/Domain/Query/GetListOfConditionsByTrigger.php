<?php

namespace App\Trigger\Domain\Query;

class GetListOfConditionsByTrigger
{
    private $triggerId;

    public function __construct(int $triggerId)
    {
        $this->triggerId = $triggerId;
    }

    public function getTriggerId(): int
    {
        return $this->triggerId;
    }


}