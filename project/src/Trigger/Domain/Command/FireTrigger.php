<?php

namespace App\Trigger\Domain\Command;

use App\Common\ValueObject\UUID;
use App\Component\Command\CommandInterface;

class FireTrigger implements CommandInterface
{
    /** @var string */
    protected $uuid;

    protected $triggerId;

    protected $data;

    public function __construct(int $eventId, \stdClass $data)
    {
        $this->uuid = UUID::generate();
        $this->triggerId = $eventId;
        $this->data = $data;
    }

    public function getTriggerId(): int
    {
        return $this->triggerId;
    }

    public function getData(): \stdClass
    {
        return $this->data;
    }

    public function getUUID(): string
    {
        return $this->uuid;
    }
}
