<?php

namespace App\Trigger\Domain\Command;

use App\Automation\Infrastructure\Repository\AutomationRepository;
use App\Component\Command\CommandHandlerAbstract;
use App\Specification\AndSpecification;
use App\Automation\Specification\TriggerElement;
use App\Automation\Specification\TriggerSpecification;
use App\Trigger\Domain\Event\TriggerSatisfied;
use App\Trigger\Domain\Model\TriggerCondition;
use Phalcon\Di;

final class FireTriggerHandler extends CommandHandlerAbstract
{
    /** @var \App\Automation\Infrastructure\Repository\AutomationRepository */
    private $automationRepository;

    public function __construct(Di $di)
    {
        parent::__construct($di);
        $this->automationRepository = new AutomationRepository($this->di->get('dbal'));
    }

    public function handle(FireTrigger $command): void
    {
        $automations = $this->automationRepository->findByTriggerId($command->getTriggerId());

        if (!$automations) {
            // no triggers = nothing to do :)
            return;
        }

        /*
         * check all triggers that have given trigger ID
         */
        $element = new TriggerElement((array)$command->getData());
        foreach ($automations as $k => $trigger) {
            $eventConditions = json_decode($trigger->getData())->conditions;
            pre($trigger);
            pre(json_decode($trigger->getData()));
            exit;
            $specs = [];

            /*
             * check each condition assigned to the trigger
             */
            foreach ($eventConditions as $condition) {
                //$condition = (array)$condition;
                $conditionVO = new TriggerCondition();
                $conditionVO->setValue($condition->value);
                $conditionVO->setId($condition->id);
                $specs[] = new TriggerSpecification($conditionVO);
            }

            /*
             * after that we should CALL another trigger
             */
            $andSpec = new AndSpecification($specs);
            if ($andSpec->isSatisfied($element)) {
                /*
                 * dispatch an trigger once conditions are satisfied
                 */
                $this->eventDispatcher->apply(new TriggerSatisfied($command->getTriggerId()));
            }
        }


        exit;
    }
}
