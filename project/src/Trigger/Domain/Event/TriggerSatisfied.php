<?php

namespace App\Trigger\Domain\Event;

use App\Common\Event\EventInterface;

final class TriggerSatisfied implements EventInterface
{
    protected $triggerId;

    public function __construct(int $triggerId)
    {
        $this->triggerId = $triggerId;
    }

    public function getTriggerId(): int
    {
        return $this->triggerId;
    }
}
