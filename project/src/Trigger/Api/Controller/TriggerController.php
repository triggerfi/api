<?php

namespace App\Trigger\Api\Controller;

use App\Common\Api\Controller\ControllerBase;
use App\Trigger\Domain\Command\FireTrigger;
use App\Trigger\Exception\EventNotFound;
use Phalcon\Http\ResponseInterface;

final class TriggerController extends ControllerBase
{
    /*
     * Events
     */
    public function listAction(): ResponseInterface
    {
        try {
            $query = new \App\Trigger\Domain\Query\GetListOfTriggers();
            $handler = new \App\Trigger\Domain\Query\GetListOfEventsHandler($this->di);
            $events = $handler->handle($query);
            $this->api->setData($events);
        } catch (\Exception $e) {
            return $this->error500($e);
        }

        return $this->respond();
    }

    /*
     * fires event
     */
    public function fireAction(int $triggerId): ResponseInterface
    {
        $data = $this->request->getJsonRawBody();

        try {
            $command = new FireTrigger($triggerId, $data);
            $this->commandBus->handle($command);
        } catch (\Exception | EventNotFound $e) {
            return $this->error500($e);
        }

        $this->api->setData($data);
        return $this->respond();
    }

    /*
     * Triggers conditions
     */
    public function conditionsAction(int $triggerId): \Phalcon\Http\ResponseInterface
    {
        try {
            $query = new \App\Trigger\Domain\Query\GetListOfConditionsByTrigger($triggerId);
            $handler = new \App\Trigger\Domain\Query\GetListOfConditionsByTriggerHandler($this->di);
            $conditions = $handler->handle($query);
            $this->response->setJsonContent(['data' => $conditions]);
        } catch (\Exception $e) {
            return $this->error500($e);
        }

        return $this->response;
    }
}
