<?php

namespace App\Trigger\Infrastructure\Repository;

use App\Trigger\Domain\Model\TriggerAggregate;
use App\Trigger\Domain\Model\TriggerCondition;
use Doctrine\DBAL\Connection;

final class TriggerRepository
{
    protected $triggers = [];

    /** @var \Doctrine\DBAL\Connection */
    private $em;

    public function __construct(Connection $manager)
    {
        $this->em = $manager;
    }

    /**
     * @param int $triggerId
     *
     * @return TriggerAggregate
     */
    public function findConditionsByTrigger(int $triggerId): TriggerAggregate
    {
        $conditions = $this->em->createQueryBuilder()
            ->select('*')
            ->from('triggers_conditions', 't')
            ->leftJoin('t', 'conditions', 'c', 't.conditions_id=c.id')
            ->where('triggers_id = ?')
            ->setParameter(0, $triggerId)
            ->execute()
            ->fetchAll();

        $agg = new TriggerAggregate();
        foreach ($conditions as $k => $v) {
            $condition = new TriggerCondition();
            $condition->setId($v->id);
            $condition->setTriggerId($v->triggers_id);
            $condition->setConditionsId($v->conditions_id);
            $condition->setName($v->name);
            $condition->setType($v->type);
            $condition->setValue(['a','sss','cc']);

            $agg->addCondition($condition);
        }

        return $agg;
    }

    public function findAll(): array
    {
        $conditions = $this->em->createQueryBuilder()
            ->select('*')
            ->from('triggers')
            ->execute()
            ->fetchAll();

        return $conditions;
    }
}
