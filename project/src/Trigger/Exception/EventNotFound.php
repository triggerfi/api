<?php

namespace App\Trigger\Exception;

class EventNotFound extends \Exception
{
    public function __construct($message = "", $code = 0, \Exception $previous = null)
    {
        $message = get_called_class() . ': ' . $message;
        parent::__construct($message, $code, $previous);
    }
}
