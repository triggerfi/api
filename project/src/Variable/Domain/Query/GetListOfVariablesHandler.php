<?php

namespace App\Variable\Domain\Query;

use App\Component\Query\QueryHandlerAbstract;
use App\Variable\Infrastructure\Repository\VariableRepository;

class GetListOfVariablesHandler extends QueryHandlerAbstract
{
    private $variableRepository;

    public function __construct(\Phalcon\Di $di)
    {
        parent::__construct($di);
        $this->variableRepository = new VariableRepository($this->di->get('dbal'));
    }

    public function handle(GetListOfVariables $query): array
    {
        $output = [];

        $variables = $this->variableRepository->findAll();

        foreach ($variables as $v) {
            $output[] = new VariableView($v);
        }

        return $output;
    }
}
