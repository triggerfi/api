<?php

namespace App\Variable\Domain\Query;

use App\Variable\Domain\Model\Variable;

final class VariableView implements \JsonSerializable
{
    /** @var int */
    private $uuid;

    /** @var string */
    private $name;

    /** @var string */
    private $default_value;

    /** @var string */
    private $value;

    /** @var int */
    private $companies_id;

    public function __construct(Variable $condition)
    {
        $this->uuid = $condition->getUuid()->getString();
        $this->name = $condition->getName();
        $this->default_value = $condition->getDefaultValue();
        $this->value = $condition->getValue();
        $this->companies_id = (int)$condition->getCompaniesId();
    }

    public function jsonSerialize()
    {
        return [
            'uuid' => $this->uuid,
            'name' => $this->name,
            'value' => $this->value
        ];
    }


}