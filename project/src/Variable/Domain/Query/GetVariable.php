<?php

namespace App\Variable\Domain\Query;

use App\Common\ValueObject\UUID;
use App\Component\Query\QueryInterface;

class GetVariable implements QueryInterface
{
    /** @var \App\Common\ValueObject\UUID */
    private $uuid;

    public function __construct(string $uuid)
    {
        $this->uuid = new UUID($uuid);
    }

    /**
     * @return \App\Common\ValueObject\UUID
     */
    public function getUuid(): \App\Common\ValueObject\UUID
    {
        return $this->uuid;
    }
}
