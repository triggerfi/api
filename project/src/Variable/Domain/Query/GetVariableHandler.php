<?php

namespace App\Variable\Domain\Query;

use App\Component\Query\QueryHandlerAbstract;
use App\Variable\Infrastructure\Repository\VariableRepository;

class GetVariableHandler extends QueryHandlerAbstract
{
    private $variableRepository;

    public function __construct(\Phalcon\Di $di)
    {
        parent::__construct($di);
        $this->variableRepository = new VariableRepository($this->di->get('dbal'));
    }

    public function handle(GetVariable $query): VariableView
    {
        $variable = $this->variableRepository->findByUuid($query->getUuid());
        $variable = new VariableView($variable);

        return $variable;
    }
}
