<?php

namespace App\Variable\Domain\Model;

use Phalcon\Text;

final class Variable
{
    private $uuid;

    private $companiesId;

    private $defaultValue;

    private $value;

    private $name;

    /**
     * @return \App\Common\ValueObject\UUID
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * @param mixed $uuid
     */
    public function setUuid($uuid): void
    {
        $this->uuid = $uuid;
    }

    /**
     * @return mixed
     */
    public function getCompaniesId()
    {
        return $this->companiesId;
    }

    /**
     * @param mixed $companiesId
     */
    public function setCompaniesId($companiesId): void
    {
        $this->companiesId = $companiesId;
    }

    /**
     * @return mixed
     */
    public function getDefaultValue()
    {
        return $this->defaultValue;
    }

    /**
     * @param mixed $defaultValue
     */
    public function setDefaultValue($defaultValue): void
    {
        $this->defaultValue = $defaultValue;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value): void
    {
        $this->value = $value;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    public static function factory(object $data): Variable
    {
        $obj = new Variable();
        foreach ($data as $k => $v) {
            $k = lcfirst(Text::camelize($k));
            if (method_exists($obj, 'set' . ucfirst($k))) {
                $obj->{'set' . ucfirst($k)}($v);
            }
        }

        return $obj;
    }
}
