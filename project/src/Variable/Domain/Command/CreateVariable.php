<?php

namespace App\Variable\Domain\Command;

use App\Common\ValueObject\UUID;
use App\Component\Command\CommandInterface;

final class CreateVariable implements CommandInterface
{
    /** @var \App\Common\ValueObject\UUID */
    private $uuid;

    /** @var string */
    private $name;

    /** @var string */
    private $value;

    public function __construct(object $json)
    {
        $this->uuid = new UUID();
        $this->name = $json->name;
        $this->value = $json->value;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @return \App\Common\ValueObject\UUID
     */
    public function getUuid(): \App\Common\ValueObject\UUID
    {
        return $this->uuid;
    }
}
