<?php

namespace App\Variable\Domain\Command;

use App\Common\ValueObject\UUID;
use App\Component\Command\CommandInterface;

final class DeleteVariable implements CommandInterface
{
    /** @var \App\Common\ValueObject\UUID */
    private $uuid;

    public function __construct($uuid)
    {
        $this->uuid = new UUID($uuid);
    }

    /**
     * @return \App\Common\ValueObject\UUID
     */
    public function getUuid(): \App\Common\ValueObject\UUID
    {
        return $this->uuid;
    }
}
