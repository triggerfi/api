<?php

namespace App\Variable\Domain\Command;

use App\Component\Command\CommandHandlerAbstract;
use App\Variable\Domain\Model\Variable;
use App\Variable\Exception\VariableRepositoryException;
use App\Variable\Infrastructure\Repository\VariableRepository;

final class DeleteVariableHandler extends CommandHandlerAbstract
{
    private $variableRepository;

    public function __construct(\Phalcon\Di $di)
    {
        parent::__construct($di);
        $this->variableRepository = new VariableRepository($this->di->get('dbal'));
    }

    /*
     * TODO: quite important to add relation check if variable is used somewhere in the system !
     */
    public function handle(DeleteVariable $command): void
    {
        try {
            $variable = new Variable();
            $variable->setUuid($command->getUuid());

            $this->variableRepository->remove($variable);
        } catch (VariableRepositoryException $e) {
            throw $e;
        }
    }
}
