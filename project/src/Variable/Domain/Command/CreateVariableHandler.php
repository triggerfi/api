<?php

namespace App\Variable\Domain\Command;

use App\Component\Command\CommandHandlerAbstract;
use App\Variable\Domain\Model\Variable;
use App\Variable\Exception\VariableRepositoryException;
use App\Variable\Infrastructure\Repository\VariableRepository;

final class CreateVariableHandler extends CommandHandlerAbstract
{
    private $variableRepository;

    public function __construct(\Phalcon\Di $di)
    {
        parent::__construct($di);
        $this->variableRepository = new VariableRepository($this->di->get('dbal'));
    }

    public function handle(CreateVariable $command): void
    {
        try {
            $variable = new Variable();
            $variable->setUuid($command->getUuid());
            $variable->setName($command->getName());
            $variable->setValue($command->getValue());
            $variable->setCompaniesId(1);

            $this->variableRepository->add($variable);
        } catch (VariableRepositoryException $e) {
            throw $e;
        }
    }
}
