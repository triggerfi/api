<?php

namespace App\Variable\Api\Controller;

use App\Common\Api\Controller\ControllerBase;
use App\Component\Api\Error;
use App\Variable\Exception\VariableRepositoryException;
use Phalcon\Http\ResponseInterface;

final class VariableController extends ControllerBase
{
    /*
     * Variables list
     */
    public function allAction(): ResponseInterface
    {
        try {
            $query = new \App\Variable\Domain\Query\GetListOfVariables();
            $handler = new \App\Variable\Domain\Query\GetListOfVariablesHandler($this->di);
            $conditions = $handler->handle($query);
            $this->api->setData($conditions);
        } catch (\Exception $e) {
            return $this->error500($e);
        }

        return $this->sendApiResponse();
    }

    /*
     * Specific variable
     */
    public function readAction(string $uuid): ResponseInterface
    {
        try {
            $query = new \App\Variable\Domain\Query\GetVariable($uuid);
            $handler = new \App\Variable\Domain\Query\GetVariableHandler($this->di);
            $conditions = $handler->handle($query);
            $this->api->setData($conditions);
        } catch (\Exception $e) {
            return $this->error500($e);
        }

        return $this->sendApiResponse();
    }

    /*
     * Delete variable
     */
    public function deleteAction(string $uuid): ResponseInterface
    {
        try {
            $command = new \App\Variable\Domain\Command\DeleteVariable($uuid);
            $this->commandBus->handle($command);
        } catch (\Exception $e) {
            return $this->error500($e);
        }

        return $this->sendApiResponse();
    }

    /*
     * Variable creation
     */
    public function createAction(): ResponseInterface
    {
        try {
            $command = new \App\Variable\Domain\Command\CreateVariable($this->request->getJsonRawBody());
            $this->commandBus->handle($command);
            $this->api->setData(['id' => $command->getUuid()->getString()]);
        } catch (VariableRepositoryException $e) {
            $error = new Error('An error occurred while creating a variable');
            $this->api->setError($error);
            return $this->sendApiResponse();
        } catch (\Exception $e) {
            return $this->error500($e);
        }

        return $this->sendApiResponse();
    }
}
