<?php

namespace App\Automation\Exception;

use Throwable;

final class VariableEntityException extends \Exception
{
    public function __construct(string $message = "", int $code = 0, \Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
