<?php


namespace App\Variable\Infrastructure\Repository;

use App\Common\ValueObject\UUID;
use App\Variable\Domain\Model\Variable;
use App\Variable\Exception\VariableRepositoryException;
use Doctrine\DBAL\Connection;

final class VariableRepository implements VariableInterface
{
    /** @var \App\Automation\Domain\Model\Automation[] */
    private $automations = [];

    /** @var \Doctrine\DBAL\Connection */
    private $em;

    public function __construct(Connection $manager)
    {
        $this->em = $manager;
    }

    public function remove(Variable $automation): void
    {
        try {
            $this->em->createQueryBuilder()
                ->delete('variables')
                ->where('uuid = ?')
                ->setParameter(0, $automation->getUuid()->getBin())
                ->execute();
        } catch (\Exception $e) {
            throw new VariableRepositoryException($e->getMessage());
        }
    }

    public function findByUuid(UUID $uuid): Variable
    {
        $var = $this->em->createQueryBuilder()
            ->select('*')
            ->from('variables')
            ->where('uuid = ?')
            ->setParameter(0, $uuid->getBin())
            ->execute()
            ->fetch();

        if (!$var) {
            throw new VariableRepositoryException('Not found');
        }

        $variable = Variable::factory($var);
        $variable->setUuid(new UUID($var->uuid));

        return $variable;
    }

    public function findAll(): array
    {
        try {
            $automations = $this->em->createQueryBuilder()
                ->select('*')
                ->from('variables')
                ->execute()
                ->fetchAll();
        } catch (\Exception $e) {
            throw new VariableRepositoryException('Error while searching', 0, $e);
        }

        foreach ($automations as $k => $v) {
            $var = Variable::factory($v);
            $var->setUuid(new UUID($v->uuid));
            $this->automations[] = $var;
        }

        return $this->automations;
    }

    public function add(Variable $variable): void
    {
        try {
            $this->em->createQueryBuilder()
                ->insert('variables')
                ->setValue('uuid', '?')
                ->setValue('companies_id', '?')
                ->setValue('name', '?')
                ->setValue('value', '?')
                ->setParameter(0, $variable->getUuid()->getBin())
                ->setParameter(1, $variable->getCompaniesId())
                ->setParameter(2, $variable->getName())
                ->setParameter(3, $variable->getValue())
                ->execute();
        } catch (\Exception $e) {
            throw new VariableRepositoryException($e->getMessage(), 0, $e);
        }
    }
}
