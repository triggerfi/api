<?php

namespace App\Variable\Infrastructure\Repository;

use App\Common\ValueObject\UUID;
use App\Variable\Domain\Model\Variable;

interface VariableInterface
{
    public function findByUuid(UUID $uuid): Variable;

    public function add(Variable $automation): void;

    public function remove(Variable $automation): void;

    public function findAll();
}
