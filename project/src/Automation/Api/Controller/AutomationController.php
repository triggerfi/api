<?php

namespace App\Automation\Api\Controller;

use App\Automation\Domain\Command\CreateAutomation;
use App\Automation\Exception\AutomationNotFoundException;
use App\Common\Api\Controller\ControllerBase;

final class AutomationController extends ControllerBase
{
    /*
     * return list of automations
     */
    public function listAction()
    {
        try {
            $query = new \App\Automation\Domain\Query\GetListOfAutomations();
            $handler = new \App\Automation\Domain\Query\GetListOfAutomationsHandler($this->di);
            $events = $handler->handle($query);
            $this->response->setJsonContent(['data' => $events]);
        } catch (\Exception $e) {
            return $this->error500($e);
        }

        return $this->response;
    }

    /*
     * return single automation
     */
    public function getAction(string $automationId)
    {
        try {
            $query = new \App\Automation\Domain\Query\GetAutomation($automationId);
            $handler = new \App\Automation\Domain\Query\GetAutomationHandler($this->di);
            $events = $handler->handle($query);
            $this->response->setJsonContent(['data' => $events]);
        } catch (AutomationNotFoundException $e) {
            return $this->notFound();
        } catch (\Exception $e) {
            return $this->error500($e);
        }

        return $this->response;
    }

    /*
     * creating new automation
     */
    public function createAction()
    {
        try {
            $command = new CreateAutomation($this->request->getJsonRawBody());
            $this->commandBus->handle($command);
            $this->response->setJsonContent([
                'data' => [
                    'id' => $command->getUuid()->getString()
                ]
            ]);
        } catch (\Exception $e) {
            return $this->error500($e);
        }

        return $this->response;
    }
}
