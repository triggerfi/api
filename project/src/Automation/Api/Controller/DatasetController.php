<?php

namespace App\Automation\Api\Controller;

use App\Automation\Domain\Command\CreateDataset;
use App\Automation\Exception\DatasetNotFoundException;
use App\Common\Api\Controller\ControllerBase;

/*
 * Controls the flow of dataset API
 */
final class DatasetController extends ControllerBase
{
    /*
     * return list of automations
     */
    public function listAction()
    {
        try {
            $query = new \App\Automation\Domain\Query\GetListOfDatasets();
            $handler = new \App\Automation\Domain\Query\GetListOfDatasetsHandler($this->di);
            $datasets = $handler->handle($query);
            $this->api->setData($datasets);
        } catch (\Exception $e) {
            return $this->error500($e);
        }

        return $this->respond();
    }

    /*
     * return documents of a dataset
     */
    public function documentsAction(string $uuid)
    {
        try {
            $query = new \App\Automation\Domain\Query\GetDatasetDocuments($uuid);
            $handler = new \App\Automation\Domain\Query\GetDatasetDocumentsHandler($this->di);
            $events = $handler->handle($query);
            $this->api->setData($events);
        } catch (DatasetNotFoundException $e) {
            return $this->notFound();
        } catch (\Exception $e) {
            return $this->error500($e);
        }

        return $this->respond();
    }

    /*
     * creating new automation
     */
    public function createAction()
    {
        try {
            $command = new CreateDataset($this->request->getJsonRawBody());
            $this->commandBus->handle($command);
            $this->api->setData(['id' => $command->getUuid()->getString()]);
        } catch (\Exception $e) {
            return $this->error500($e);
        }

        return $this->respond();
    }
}
