<?php

namespace App\Automation\Domain\Command;

use App\Automation\Domain\Model\Automation;
use App\Automation\Exception\AutomationEntityException;
use App\Automation\Exception\AutomationRepositoryException;
use App\Automation\Infrastructure\Repository\AutomationRepository;
use App\Component\Command\CommandHandlerAbstract;
use Phalcon\Di;

final class CreateAutomationHandler extends CommandHandlerAbstract
{
    private $automationRepository;

    public function __construct(Di $di)
    {
        parent::__construct($di);
        $this->automationRepository = new AutomationRepository($this->di->get('dbal'));
    }

    public function handle(CreateAutomation $command): void
    {
        try {
            $automation = new Automation();
            $automation->setData(\json_encode($command->getData()));
            $automation->setUuid($command->getUuid()->getString());
            $automation->setName($command->getName());
            $automation->setTriggerId($command->getTriggerId());

            $this->automationRepository->add($automation);
        } catch (AutomationEntityException $e) {
            // log
            throw $e;
        } catch (AutomationRepositoryException $e) {
            // log
            throw $e;
        }
    }
}
