<?php

namespace App\Automation\Domain\Command;

use App\Component\Command\CommandInterface;
use App\Common\ValueObject\UUID;

final class CreateDataset implements CommandInterface
{
    /** @var int */
    private $companyId;

    /** @var \App\Common\ValueObject\UUID */
    private $uuid;

    /** @var string */
    private $name;

    public function __construct(\stdClass $json)
    {
        $this->companyId = 1;
        $this->uuid =  new \App\Common\ValueObject\UUID();
        $this->name = $json->name;
    }

    /**
     * @return int
     */
    public function getCompanyId(): int
    {
        return $this->companyId;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return \App\Common\ValueObject\UUID
     */
    public function getUuid(): UUID
    {
        return $this->uuid;
    }
}
