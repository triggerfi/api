<?php

namespace App\Automation\Domain\Command;

use App\Component\Command\CommandInterface;
use App\Common\ValueObject\UUID;

final class CreateAutomation implements CommandInterface
{
    /** @var \stdClass */
    private $data;

    /** @var \App\Common\ValueObject\UUID */
    private $uuid;

    /** @var int */
    private $triggerId;

    /** @var string */
    private $name;

    public function __construct(\stdClass $json)
    {
        $this->data = $json;
        $this->uuid =  new \App\Common\ValueObject\UUID();
        $this->triggerId = (int)$json->event->id;
        $this->name = $json->name;
    }

    /**
     * @return \stdClass
     */
    public function getData(): \stdClass
    {
        return $this->data;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return \App\Common\ValueObject\UUID
     */
    public function getUuid(): UUID
    {
        return $this->uuid;
    }

    public function getTriggerId(): int
    {
        return $this->triggerId;
    }
}
