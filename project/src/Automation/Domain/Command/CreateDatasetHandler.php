<?php

namespace App\Automation\Domain\Command;

use App\Automation\Domain\Model\Dataset;
use App\Automation\Exception\DatasetEntityException;
use App\Automation\Exception\DatasetRepositoryException;
use App\Automation\Infrastructure\Repository\DatasetRepository;
use App\Component\Command\CommandHandlerAbstract;
use Phalcon\Di;

final class CreateDatasetHandler extends CommandHandlerAbstract
{
    private $datasetRepository;

    public function __construct(Di $di)
    {
        parent::__construct($di);
        $this->datasetRepository = new DatasetRepository($this->di->get('mongo'));
    }

    public function handle(CreateDataset $command): void
    {
        try {
            $dataset = new Dataset();
            $dataset->setUuid($command->getUuid()->getString());
            $dataset->setName($command->getName());
            $dataset->setCompanyId($command->getCompanyId());

            $this->datasetRepository->add($dataset);
        } catch (DatasetEntityException $e) {
            // log
            throw $e;
        } catch (DatasetRepositoryException $e) {
            // log
            throw $e;
        }
    }
}
