<?php

namespace App\Automation\Domain\Query;

use App\Common\ValueObject\UUID;

final class DatasetView implements \JsonSerializable
{
    /** @var \App\Common\ValueObject\UUID */
    private $uuid;

    private $name;

    public function __construct(UUID $uuid, string $name)
    {
        $this->uuid = $uuid;
        $this->name = $name;
    }

    public function jsonSerialize()
    {
        return [
            'uuid' => $this->uuid->getString(),
            'name' => $this->name
        ];
    }
}