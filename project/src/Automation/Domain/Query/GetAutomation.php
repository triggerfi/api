<?php

namespace App\Automation\Domain\Query;

use App\Common\ValueObject\UUID;

final class GetAutomation
{
    /** @var \App\Common\ValueObject\UUID */
    private $uuid;

    public function __construct(string $uuid)
    {
        $this->uuid = new UUID($uuid);
    }

    /**
     * @return UUID
     */
    public function getUuid(): UUID
    {
        return $this->uuid;
    }
}
