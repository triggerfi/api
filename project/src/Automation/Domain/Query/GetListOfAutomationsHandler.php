<?php

namespace App\Automation\Domain\Query;

use App\Automation\Infrastructure\Repository\AutomationRepository;
use App\Component\Query\QueryHandlerAbstract;
use Phalcon\Di;

class GetListOfAutomationsHandler extends QueryHandlerAbstract
{
    private $automationRepository;

    public function __construct(Di $di)
    {
        parent::__construct($di);
        $this->automationRepository = new AutomationRepository($this->di->get('dbal'));
    }

    /*
     * Return the list of all automations filtered by company
     */
    public function handle(GetListOfAutomations $query): array
    {
        $out = [];
        $data = $this->automationRepository->findAll();
        foreach ($data as $k => $v) {
            $out[] = new AutomationView($v, true);
        }

        return $out;
    }
}
