<?php

namespace App\Automation\Domain\Query;

use App\Common\ValueObject\UUID;

class GetDatasetDocuments
{
    private $uuid;

    public function __construct(string $uuid)
    {
        $this->uuid = new UUID($uuid);
    }

    /**
     * @return UUID
     */
    public function getUuid(): UUID
    {
        return $this->uuid;
    }
}
