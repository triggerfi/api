<?php

namespace App\Automation\Domain\Query;

use App\Automation\Infrastructure\Repository\DatasetRepository;
use App\Component\Query\QueryHandlerAbstract;
use Phalcon\Di;

class GetDatasetDocumentsHandler extends QueryHandlerAbstract
{
    private $datasetRepository;

    public function __construct(Di $di)
    {
        parent::__construct($di);
        $this->datasetRepository = new DatasetRepository($this->di->get('mongo'));
    }

    /*
     * Return the list of all automations filtered by company
     */
    public function handle(GetDatasetDocuments $query): array
    {
        $out = [];
        $data = $this->datasetRepository->findDatasetDocuments($query->getUuid());
        foreach ($data as $k => $v) {
            $out[] = new DatasetDocumentView($v);
        }

        return $out;
    }
}
