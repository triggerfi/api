<?php

namespace App\Automation\Domain\Query;

use App\Automation\Domain\Model\Automation;

final class AutomationView implements \JsonSerializable
{
    /** @var \App\Common\ValueObject\UUID */
    private $uuid;

    private $name;

    private $triggerId;

    private $dataJson;

    public function __construct(Automation $model, bool $includeData = false)
    {
        $this->uuid = $model->getUUID();
        $this->name = $model->getName();
        $this->triggerId = $model->getTriggerId();
        $this->dataJson = $includeData ? $model->getData() : null;
    }

    /**
     * @return mixed
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getTriggerId(): int
    {
        return $this->triggerId;
    }

    /**
     * @return null|string
     */
    public function getDataJson(): ?string
    {
        return $this->dataJson;
    }

    public function jsonSerialize()
    {
        return [
            'uuid' => $this->uuid->getString(),
            'name' => $this->name,
            'eventId' => $this->triggerId,
            'dataJson' => $this->dataJson
        ];
    }
}