<?php

namespace App\Automation\Domain\Query;

use App\Automation\Domain\Model\DatasetDocument;

final class DatasetDocumentView implements \JsonSerializable
{
    /** @var \App\Common\ValueObject\UUID */
    private $uuid;

    private $parentUuid;

    private $data;

    public function __construct(DatasetDocument $document)
    {
        $this->uuid = $document->getUuid();
        $this->parentUuid = $document->getParentUuid();
        $this->data = $document->getData();
    }

    public function jsonSerialize()
    {
        return [
            'uuid' => $this->uuid->getString(),
            'parentUuid' => $this->parentUuid->getString(),
            'data' => $this->data
        ];
    }
}