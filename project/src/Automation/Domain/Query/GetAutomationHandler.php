<?php

namespace App\Automation\Domain\Query;

use App\Automation\Exception\AutomationNotFoundException;
use App\Automation\Infrastructure\Repository\AutomationRepository;
use App\Component\Query\QueryHandlerAbstract;
use Phalcon\Di;

class GetAutomationHandler extends QueryHandlerAbstract
{
    private $automationRepository;

    public function __construct(Di $di)
    {
        parent::__construct($di);
        $this->automationRepository = new AutomationRepository($this->di->get('dbal'));
    }

    public function handle(GetAutomation $query): AutomationView
    {
        $data = $this->automationRepository->findByUuid($query->getUuid());

        return new AutomationView($data, true);
    }
}
