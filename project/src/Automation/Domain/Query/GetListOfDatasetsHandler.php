<?php

namespace App\Automation\Domain\Query;

use App\Automation\Infrastructure\Repository\DatasetRepository;
use App\Component\Query\QueryHandlerAbstract;
use Phalcon\Di;

class GetListOfDatasetsHandler extends QueryHandlerAbstract
{
    private $datasetRepository;

    public function __construct(Di $di)
    {
        parent::__construct($di);
        $this->datasetRepository = new DatasetRepository($this->di->get('mongo'));
    }

    /*
     * Return the list of all automations filtered by company
     */
    public function handle(GetListOfDatasets $query): array
    {
        $out = [];
        $data = $this->datasetRepository->findAll();
        foreach ($data as $k => $v) {
            $out[] = new DatasetView($v->getUuid(), $v->getName());
        }

        return $out;
    }
}
