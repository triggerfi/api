<?php

namespace App\Automation\Domain\Model;

use Phalcon\Text;

final class Automation
{
    /** @var \App\Common\ValueObject\UUID */
    private $uuid;

    private $name;

    private $triggerId;

    private $data;

    /**
     * @param string
     */
    public function setUuid(string $uuid): void
    {
        $this->uuid = new \App\Common\ValueObject\UUID($uuid);
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @param int $triggerId
     */
    public function setTriggerId(int $triggerId): void
    {
        $this->triggerId = $triggerId;
    }

    /**
     * @param string $data
     */
    public function setData(string $data): void
    {
        $this->data = $data;
    }

    /**
     * @return \App\Common\ValueObject\UUID
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getTriggerId(): int
    {
        return $this->triggerId;
    }

    /**
     * @return null|string
     */
    public function getData(): ?string
    {
        return $this->data;
    }

    public static function factory(object $data): Automation
    {
        $obj = new Automation();
        foreach ($data as $k => $v) {
            $k = lcfirst(Text::camelize($k));
            if (method_exists($obj, 'set' . ucfirst($k))) {
                $obj->{'set' . ucfirst($k)}($v);
            }
        }

        return $obj;
    }
}
