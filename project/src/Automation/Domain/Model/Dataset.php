<?php

namespace App\Automation\Domain\Model;

final class Dataset
{
    /** @var \App\Common\ValueObject\UUID */
    private $uuid;

    /** @var string */
    private $name;

    /** @var int */
    private $companyId;

    /**
     * @param string
     */
    public function setUuid(string $uuid): void
    {
        $this->uuid = new \App\Common\ValueObject\UUID($uuid);
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getCompanyId(): int
    {
        return $this->companyId;
    }

    /**
     * @param int $companyId
     */
    public function setCompanyId(int $companyId): void
    {
        $this->companyId = $companyId;
    }

    /**
     * @return \App\Common\ValueObject\UUID
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
}
