<?php

namespace App\Automation\Domain\Model;

final class DatasetDocument
{
    /** @var \App\Common\ValueObject\UUID */
    private $uuid;

    /** @var \App\Common\ValueObject\UUID */
    private $parentUuid;

    /** @var array */
    private $data;

    /**
     * @return \App\Common\ValueObject\UUID
     */
    public function getUuid(): \App\Common\ValueObject\UUID
    {
        return $this->uuid;
    }

    /**
     * @param \App\Common\ValueObject\UUID $uuid
     */
    public function setUuid(\App\Common\ValueObject\UUID $uuid): void
    {
        $this->uuid = $uuid;
    }

    /**
     * @return \App\Common\ValueObject\UUID
     */
    public function getParentUuid(): \App\Common\ValueObject\UUID
    {
        return $this->parentUuid;
    }

    /**
     * @param \App\Common\ValueObject\UUID $parentUuid
     */
    public function setParentUuid(\App\Common\ValueObject\UUID $parentUuid): void
    {
        $this->parentUuid = $parentUuid;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData(array $data): void
    {
        $this->data = $data;
    }
}
