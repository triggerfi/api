<?php

namespace App\Automation\Infrastructure\Repository;

use App\Automation\Domain\Model\Automation;
use App\Common\ValueObject\UUID;

interface AutomationInterface
{
    /**
     * @param int $triggerId
     *
     * @return Automation[]
     */
    public function findByTriggerId(int $triggerId): array;

    public function findByUuid(UUID $uuid): Automation;

    public function add(Automation $automation): void;

    public function remove(Automation $automation): void;

    /**
     * @return Automation[]
     */
    public function findAll(): array;
}
