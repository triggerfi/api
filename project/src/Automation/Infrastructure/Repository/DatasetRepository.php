<?php


namespace App\Automation\Infrastructure\Repository;

use App\Automation\Domain\Model\Dataset;
use App\Automation\Domain\Model\DatasetDocument;
use App\Automation\Exception\DatasetRepositoryException;
use App\Common\ValueObject\UUID;
use MongoDB\Database;

final class DatasetRepository implements DatasetInterface
{
    /** @var Database */
    private $em;

    public function __construct(Database $db)
    {
        $this->em = $db;
    }

    public function remove(Dataset $dataset): void
    {
        // TODO: Implement remove() method.
    }

    public function findByUuid(UUID $uuid): Dataset
    {
        //
    }

    /**
     * @param int $companyId
     *
     * @return Dataset[]
     */
    public function findAllByCompany(int $companyId): array
    {
        $data = $this->em->selectCollection('datasets_names')
            ->find(['companies_id' => 1])
            ->toArray();

        $out = [];

        foreach ($data as $k => $v) {
            /** @var \MongoDB\Model\BSONDocument $v */
            $dataset = new Dataset();
            $dataset->setUuid($v->_id);
            $dataset->setName($v->name);
            $dataset->setCompanyId($companyId);

            $out[] = $dataset;
        }

        return $out;
    }

    public function findDatasetDocuments(UUID $uuid): array
    {
        $data = $this->em->selectCollection('datasets')
            ->find(['dataset_name.$id' => $uuid->getSimplified()])
            ->toArray();

        $out = [];

        foreach ($data as $k => $v) {
            $v = get_object_vars($v);
            $dataset = new DatasetDocument();
            $dataset->setUuid(new UUID($v['_id']));
            $dataset->setParentUuid(new UUID($v['dataset_name']['$id']));

            unset($v['dataset_name']);
            unset($v['_id']);
            $dataset->setData($v);

            $out[] = $dataset;
        }

        return $out;
    }

    /**
     * @return Dataset[]
     */
    public function findAll(): array
    {
        $data = $this->em->selectCollection('datasets_names')
            ->find(['companies_id' => 1])
            ->toArray();

        $out = [];

        foreach ($data as $k => $v) {
            /** @var \MongoDB\Model\BSONDocument $v */
            $dataset = new Dataset();
            $dataset->setUuid($v->_id);
            $dataset->setName($v->name);
            $dataset->setCompanyId(1);

            $out[] = $dataset;
        }

        return $out;
    }

    public function add(Dataset $dataset): void
    {
        try {
            $this->em->selectCollection('datasets_names')
                ->insertOne([
                    '_id' => $dataset->getUuid()->getSimplified(),
                    'name' => $dataset->getName(),
                    'companies_id' => $dataset->getCompanyId()
                ]);
        } catch (\Exception $e) {
            throw new DatasetRepositoryException($e->getMessage(), 0, $e);
        }
    }
}
