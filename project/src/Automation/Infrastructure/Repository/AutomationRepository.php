<?php


namespace App\Automation\Infrastructure\Repository;

use App\Automation\Domain\Model\Automation;
use App\Automation\Exception\AutomationNotFoundException;
use App\Automation\Exception\AutomationRepositoryException;
use App\Common\ValueObject\UUID;
use Doctrine\DBAL\Connection;

final class AutomationRepository implements AutomationInterface
{
    /** @var \App\Automation\Domain\Model\Automation[] */
    private $automations = [];

    /** @var \Doctrine\DBAL\Connection */
    private $em;

    public function __construct(Connection $manager)
    {
        $this->em = $manager;
    }

    public function remove(Automation $automation): void
    {
        // TODO: Implement remove() method.
    }

    /**
     * @param int $triggerId
     *
     * @return Automation[]
     */
    public function findByTriggerId(int $triggerId): array
    {
        $automations = $this->em->createQueryBuilder()
            ->select('*')
            ->from('automations')
            ->where('trigger_id = ?')
            ->setParameter(0, $triggerId)
            ->execute()
            ->fetchAll();

        $out = [];
        foreach ($automations as $k => $v) {
            $out[] = Automation::factory($v);
        }

        return $out;
    }

    public function findByUuid(UUID $uuid): Automation
    {
        $automation = $this->em->createQueryBuilder()
            ->select('*')
            ->from('automations')
            ->where('uuid = ?')
            ->setParameter(0, $uuid->getBin())
            ->execute()
            ->fetch();

        if (!$automation) {
            throw new AutomationNotFoundException('Automation not found with UUID ' . $uuid->getString());
        }

        return Automation::factory($automation);
    }

    /**
     * @return array
     * @throws \App\Automation\Exception\AutomationRepositoryException
     */
    public function findAll(): array
    {
        try {
            $automations = $this->em->createQueryBuilder()
                ->select('*')
                ->from('automations')
                ->execute()
                ->fetchAll();
        } catch (\Exception $e) {
            throw new AutomationRepositoryException('Error while searching', 0, $e);
        }

        foreach ($automations as $k => $v) {
            $this->automations[] = Automation::factory($v);
        }

        return $this->automations;
    }

    public function add(Automation $automation): void
    {
        try {
            $this->em->createQueryBuilder()
                ->insert('automations')
                ->setValue('uuid', '?')
                ->setValue('trigger_id', '?')
                ->setValue('companies_id', 1)
                ->setValue('name', '?')
                ->setValue('data', '?')
                ->setParameter(0, $automation->getUuid()->getBin())
                ->setParameter(1, $automation->getTriggerId())
                ->setParameter(2, $automation->getName())
                ->setParameter(3, ($automation->getData()))
                ->execute();
        } catch (\Exception $e) {
            throw new AutomationRepositoryException('Error saving entity', 0, $e);
        }
    }
}
