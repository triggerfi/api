<?php

namespace App\Automation\Infrastructure\Repository;

use App\Automation\Domain\Model\Dataset;
use App\Common\ValueObject\UUID;

interface DatasetInterface
{
    public function findByUuid(UUID $uuid): Dataset;

    public function add(Dataset $dataset): void;

    public function remove(Dataset $automation): void;

    public function findAll();
}
