<?php

namespace App\Automation\Specification;

use App\Specification\AbstractSpecification;
use App\Specification\SpecificationElement;
use App\Trigger\Domain\Model\TriggerCondition;

final class TriggerSpecification extends AbstractSpecification
{
    /** @var \App\Trigger\Domain\Model\TriggerCondition */
    protected $data;

    public function __construct(TriggerCondition $data)
    {
        $this->data = $data;
    }

    public function isSatisfied(SpecificationElement $element): bool
    {
        return self::compare($this->data->getValue(), $this->data['sign'], $element->get($this->data->getName()));
    }
}
