<?php

namespace App\Automation\Specification;

use App\Specification\SpecificationElement;

final class TriggerElement extends SpecificationElement
{
    public function __construct(array $obj)
    {
        $this->data = $obj;
    }
}
