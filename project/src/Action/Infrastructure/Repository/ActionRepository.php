<?php

namespace App\Action\Infrastructure\Repository;

use App\Action\Domain\Model\Action;
use Doctrine\DBAL\Connection;

final class ActionRepository implements ActionInterface
{
    private $em;

    public function __construct(Connection $manager)
    {
        $this->em = $manager;
    }

    public function findById(int $id): Action
    {
        $automation = $this->em->createQueryBuilder()
            ->select('*')
            ->from('action')
            ->where('uuid = ?')
            ->setParameter(0, $id)
            ->execute()
            ->fetch();

        return Action::factory($automation);
    }

    public function add(Action $automation): void
    {
        // TODO: Implement add() method.
    }

    public function remove(Action $automation): void
    {
        // TODO: Implement remove() method.
    }

    /**
     * @return Action[]
     */
    public function findAll(): array
    {
        $actions = $this->em->createQueryBuilder()
            ->select('*')
            ->from('actions')
            ->execute()
            ->fetchAll();

        $out = [];
        foreach ($actions as $k => $v) {
            $out[] = Action::factory($v);
        }

        return $out;
    }
}
