<?php

namespace App\Action\Infrastructure\Repository;

use App\Action\Domain\Model\Action;

interface ActionInterface
{
    public function findById(int $id): Action;

    public function add(Action $automation): void;

    public function remove(Action $automation): void;

    public function findAll();
}