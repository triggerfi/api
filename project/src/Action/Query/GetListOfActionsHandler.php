<?php

namespace App\Action\Query;

use App\Action\Infrastructure\Repository\ActionRepository;
use App\Component\Query\QueryHandlerAbstract;

class GetListOfActionsHandler extends QueryHandlerAbstract
{
    private $actionRepository;

    public function __construct(\Phalcon\Di $di)
    {
        parent::__construct($di);
        $this->actionRepository = new ActionRepository($di->get('dbal'));
    }

    public function handle(GetListOfActions $query): array
    {
        $output = [];

        $data = $this->actionRepository->findAll();

        foreach ($data as $v) {
            $output[] = new ActionView($v->getId(), $v->getName());
        }

        return $output;
    }
}