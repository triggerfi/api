<?php

namespace App\Component\Command;

use Phalcon\Di;

/**
 * Class CommandHandlerAbstract
 *
 * @method handle(CommandInterface $command)
 * @package App\Command
 */
abstract class CommandHandlerAbstract
{
    /** @var \App\Common\Event\Dispatcher $dispatcher */
    protected $eventDispatcher;

    /** @var \Phalcon\Di */
    protected $di;

    public function __construct(Di $di)
    {
        $this->di = $di;
        $this->eventDispatcher = $this->di->get('ed');
    }
}
