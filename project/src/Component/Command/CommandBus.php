<?php


namespace App\Component\Command;

use Phalcon\Di;

class CommandBus
{
    /** @var \App\Common\Event\Dispatcher */
    protected $dispatcher;

    /** @var \Phalcon\Di */
    protected $di;

    public function __construct(Di $di)
    {
        $this->di = $di;
        $this->dispatcher = $this->di->get('ed');
    }

    /**
     * @param \App\Component\Command\CommandInterface $command
     *
     * @throws \App\Component\Command\CommandException
     */
    public function handle(CommandInterface $command): void
    {
        $handlerClassName = get_class($command) . 'Handler';
        if (!class_exists($handlerClassName)) {
            throw new CommandException('Handler for ' . get_class($command) . ' not found');
        }

        /** @var \App\Component\Command\CommandHandlerAbstract $handler */
        $handler = new $handlerClassName($this->di);
        $handler->handle($command);

        $this->dispatcher->fire();
    }
}
