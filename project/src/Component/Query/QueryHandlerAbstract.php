<?php

namespace App\Component\Query;

use Phalcon\Di;

/**
 * Class QueryHandlerAbstract
 *
 * @method handle(QueryInterface $command)
 * @package App\Command
 */
abstract class QueryHandlerAbstract
{
    /** @var \App\Common\Event\Dispatcher $dispatcher */
    protected $eventDispatcher;

    /** @var \Phalcon\Di */
    protected $di;

    public function __construct(Di $di)
    {
        $this->di = $di;
        $this->eventDispatcher = $this->di->get('ed');
    }
}
