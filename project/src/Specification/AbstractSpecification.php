<?php

namespace App\Specification;

abstract class AbstractSpecification implements SpecificationInterface
{
    public static function compare(string $one, string $sign, string $two): bool
    {
        switch ($sign) {
            case 'eq':
                return (bool)($one == $two);
            case 'ne':
                return (bool)($one != $two);
            case 'lt':
                return (bool)($one < $two);
            case 'lte':
                return (bool)($one <= $two);
            case 'gt':
                return (bool)($one > $two);
            case 'gte':
                return (bool)($one >= $two);
            default:
                throw new \InvalidArgumentException('Wrong sign ' . $sign);
        }
    }
}
