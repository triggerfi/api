<?php

namespace App\Specification;

abstract class SpecificationElement
{
    protected $data;

    public function get(string $key)
    {
        return $this->data[$key] ?? null;
    }
}
