<?php

namespace App\Specification;

/**
 * Interface SpecificationInterface
 *
 * @package Lib\Specification
 */
interface SpecificationInterface
{
    /**
     * @param \App\Specification\SpecificationElement $element
     *
     * @return bool
     */
    public function isSatisfied(SpecificationElement $element): bool;
}