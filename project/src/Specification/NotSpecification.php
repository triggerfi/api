<?php

namespace App\Specification;

/**
 * Class NotSpecification
 *
 * @package Lib\Specification
 */
class NotSpecification implements SpecificationInterface
{
    /**
     * @var SpecificationInterface
     */
    private $specification;

    /**
     * NotSpecification constructor.
     *
     * @param \App\Specification\SpecificationInterface $specification
     */
    public function __construct(SpecificationInterface $specification)
    {
        $this->specification = $specification;
    }

    /**
     * @param \App\Specification\SpecificationElement $item
     *
     * @return bool
     */
    public function isSatisfied(SpecificationElement $item): bool
    {
        return !$this->specification->isSatisfied($item);
    }
}