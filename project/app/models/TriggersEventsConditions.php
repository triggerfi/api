<?php

namespace Models;

use App\Conditions\Elements\ConditionFactory;
use App\Conditions\Exception\InvalidCondition;
use Phalcon\Text;

/**
 * Class TriggersEventsConditions
 */
final class TriggersEventsConditions extends \Phalcon\Mvc\Model
{
    public $id;

    public $eventId;

    public $name;

    public $code;

    public function initialize()
    {
        $this->setSchema('gamifyit');
        $this->setSource('automations_triggers_conditions');
        $this->belongsTo('event_id', '\TriggersEvents', 'id', ['alias' => 'TriggersEvents']);
    }

    public function columnMap()
    {
        $columns = $this->getModelsMetaData()->getAttributes($this);
        $map = [];
        foreach ($columns as $column) {
            $map[$column] = lcfirst(Text::camelize($column));
        }

        return $map;
    }

    public static function getByEventId(int $eventId): array
    {
        $data = self::find([
            'triggerId = :id:',
            'bind' => [
                'id' => $eventId
            ]
        ]);

        try {
            $triggerConditions = [];
            foreach ($data as $k => $v) {
                $triggerConditions[] = ConditionFactory::create($v->id, $v->code);
            }
        } catch (InvalidCondition $e) {
            // log fuck up
        }

        \App\Common\Helpers\Sort::reindexByKey($triggerConditions, 'id');

        return $triggerConditions;
    }
}
