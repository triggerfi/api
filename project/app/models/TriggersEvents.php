<?php

namespace Models;

/**
 * Class TriggersEvents
 */
final class TriggersEvents extends \Phalcon\Mvc\Model
{
    public $id;

    public $name;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSource('automations_triggers');
    }
}
