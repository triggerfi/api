<?php

namespace Models;

use Phalcon\Di;
use Phalcon\Di\Injectable;

final class Factory extends Injectable
{
    public static function create(string $name)
    {
        $entities = Di::getDefault()->get('entities');
        if (!isset($entities[$name])) {
            throw new \InvalidArgumentException('Model ' . $name . ' not found');
        }

        return new $entities[$name];
    }
}
